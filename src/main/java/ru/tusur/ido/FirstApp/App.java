package ru.tusur.ido.FirstApp;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException
    {
        System.out.println( "Hello World!" );
        Thread.sleep(5000);
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();
        System.out.println(name);
    }
}
